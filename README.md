# JamesBeers

Aplicativo de cervejas para James

## Getting Started

XCode Version 11.3.1

Swift 5

Aplicativo feito para iPhone

Arquitetura MVVM

O aplicativo está em inglês conforme a API retorna os dados em inglês

Arquivos source das artes do aplicativo em .psd

## Pods utilizados

- NVActivityIndicatorView
- Toast-Swift
- SwiftyJSON
- Alamofire
- AttributedTextView
- SDWebImage
  
## Unit Tests

Foram feitos alguns testes unitários para o aplicativo

## Características

Tratamento de erro para carregamento inicial das cervejas

Tratamento de erro para carregar mais cervejas conforme chega no final da lista

Tratamento para configuração de tela Light ou Dark

## Demonstração

- Lista de cervejas, detalhes da cerveja e Light / Dark Mode

![Alt text](https://media.giphy.com/media/S6IDB3t4Zh6FeCLBCD/giphy.gif)

- Tratamento de erro de conexão

![Alt text](https://media.giphy.com/media/U452dd8KcS84Ri8act/giphy.gif)

## Autor

* **Eduardo Maia** - *dudisitchy@gmail.com*