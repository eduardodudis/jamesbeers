//
//  Beer.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation

// MARK: - Model

struct Beer {
  
    public let id: Int?
    public let name: String?
    public let tagline: String?
    public let image: String?
    public let ingredients: Ingredients?
    
    init(id: Int, name: String, tagline: String, image: String, ingredients: Ingredients) {
        
        self.id = id
        self.name = name
        self.tagline = tagline
        self.image = image
        self.ingredients = ingredients
        
    }
}

// MARK: - Model Support

struct Ingredients {
    
    let malt: Malt
    let hops: [Hops]
    let yeast: String
    
}

struct Malt {
    
    let name: String
    let value: String
    let unit: String
    
}

struct Hops {
    
    let name: String
    let value: String
    let unit: String
    let add: String
    let attribute: String
    
}
