//
//  BeerViewModel.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation
import UIKit

// MARK: - ViewModel

class BeerViewModel {
    
    let apiManager = MockAPI()
    
    var reloadList = {() -> () in }
    
    var singleBeer = [String: String]()
    
    var beerViewModel : [Beer] = [] {
        
        // Reload list when data set
        didSet {
            reloadList()
        }
        
    }
    
    // Call API and send JSON to parse
    func callApi() {
        
        apiManager.getAllBeers(for: (GlobalVariables.instance().apiBaseUrl) + GlobalVariables.instance().apiBeers + "\(GlobalVariables.instance().page)" + GlobalVariables.instance().apiPerPage + "\(GlobalVariables.instance().beersLimitPage)", result: { json in
            
            if json != nil {
                self.beerViewModel = self.apiManager.fillBeers(json: json)
            }
            else {
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "callAPIError"), object: nil)
            }
                                        
        })
        
    } // end api func
    
    // Return the complete info of a beer to use on detail view
    func getSingleBeer(index: Int) -> [String: String] {
        
        singleBeer["name"] = beerViewModel[index].name
        singleBeer["image"] = beerViewModel[index].image
        
        singleBeer["ingredients_malt_name"] = beerViewModel[index].ingredients?.malt.name
        singleBeer["ingredients_malt_value"] = beerViewModel[index].ingredients?.malt.value
        singleBeer["ingredients_malt_unit"] = beerViewModel[index].ingredients?.malt.unit

        var hops: String = ""
        
        // Build a string of hops to show in detail view
        for hop in beerViewModel[index].ingredients!.hops {
            hops += hop.name + "\n" + hop.value + " " + hop.unit + "\n" + NSLocalizedString("add", comment: "")  + ": " + hop.add + "\n" + NSLocalizedString("attribute", comment: "") + ": " + hop.attribute + "\n\n"
        }
        
        singleBeer["ingredients_hops"] = hops
        
        singleBeer["ingredients_yeast"] = beerViewModel[index].ingredients?.yeast
        
        return singleBeer
        
    } // end single beer func

} // end class BeerViewModel
