//
//  RequestProtocol.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation

protocol RequestProtocol {
    
    func getAllBeers(for url: String, result: @escaping (_ json: Data?) -> ())
    
    func fillBeers(json: Data?) -> [Beer]
    
}
