//
//  VCBeers.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import Toast_Swift

// MARK: - Beers View Controller

class VCBeers: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var buttonTryAgain: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var textError: UILabel!
    @IBOutlet weak var loadingActivity: NVActivityIndicatorView!
    
    let deafultImage = UIImage(named: "placeholder")
    
    var viewModel = BeerViewModel()
    var encodedUrl: URL!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // config Navigation Bar custom
        navigationController?.navigationBar.barTintColor = GlobalVariables.instance().colorPrimary
        
        let attributes = [NSAttributedString.Key.font: UIFont(name: "Lato-Black", size: 19)!]
        UINavigationBar.appearance().titleTextAttributes = attributes
        
        textError.text = NSLocalizedString("error_loading_message", comment: "")
        
        // Listen to API error connection
        NotificationCenter.default.addObserver(self, selector: #selector(handleError(notification:)), name: Notification.Name("callAPIError"), object: nil)
        
        // Table View init
        tableView.delegate = self
        tableView.dataSource = self
        
        // Check user mobile mode
        if #available(iOS 13.0, *) {
            changeUIColors()
        }
        
        // Call settings method
        initialMethod()
        
        // Setup data
        pageSetup()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        
        // Dynamic row height config
        tableView.estimatedRowHeight = 300
        tableView.rowHeight = UITableView.automaticDimension
        
    }

    override func didReceiveMemoryWarning() {
        
        super.didReceiveMemoryWarning()
        
    }
    
    // Check user mobile mode
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        super.traitCollectionDidChange(previousTraitCollection)

        if #available(iOS 13.0, *) {
            changeUIColors()
        }
        
    }
    
    // Change UI colors for dark or light mode
    func changeUIColors() {
        
        if self.traitCollection.userInterfaceStyle == .dark {
            mainView.backgroundColor = UIColor.black
        }
        else {
            mainView.backgroundColor = GlobalVariables.instance().colorGrey
        }
        
    }
    
    // Initial page settings
    func initialMethod() {
        
        tableView.isHidden = true
        mainView.isHidden = false
        buttonTryAgain.isHidden = true
        buttonTryAgain.isEnabled = false
        infoView.isHidden = true
        loadingActivity.startAnimating()
        
    }
    
    // Fill page
    func pageSetup()  {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            // API calling from viewmodel class
            self.viewModel.callApi()
            self.closureSetUp()
        }
        
    }
    
    func closureSetUp()  {
        
        viewModel.reloadList = { [weak self] ()  in
            // UI changes in main tread
            DispatchQueue.main.async {
                self?.tableView.reloadData()
                self?.tableView.isHidden = false
                self?.mainView.isHidden = true
                self?.buttonTryAgain.isEnabled = false
            }
        }
        
    }
    
    // Handle errors from API calls
    @objc func handleError(notification: NSNotification) {
        
        if viewModel.beerViewModel.count == 0 {
            tableView.isHidden = true
            mainView.isHidden = false
            buttonTryAgain.isHidden = false
            buttonTryAgain.isEnabled = true
            infoView.isHidden = false
            loadingActivity.stopAnimating()
        }
        else {
            self.view.makeToast(NSLocalizedString("error_toast_message", comment: ""), duration: 3.0, position: .bottom)
        }
        
    }
    
    // Reload API call
    @IBAction func tryAgainAction(_ sender: Any) {
        
        initialMethod()
        pageSetup()
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "detailSegue" {
            // Prepare data and send to VCBeerDetail
            if let destinationViewController = segue.destination as? VCBeerDetail {
                let indexPath = self.tableView.indexPathForSelectedRow!
                let index = indexPath.row
                
                guard let cell = sender as? BeerTableViewCell else {
                    return
                }
                
                guard let beerImage = cell.urlToImage.image else {
                    return
                }
                
                destinationViewController.beer = viewModel.getSingleBeer(index: index)
                destinationViewController.image = beerImage
            }
        }
        
    } // end prepare func

} // end VCBeers class
