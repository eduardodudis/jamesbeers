//
//  VCBeerDetail.swift
//  James Beers
//
//  Created by Eduardo Maia on 25/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import UIKit
import AttributedTextView

class VCBeerDetail: UIViewController {

    @IBOutlet weak var imgBeer: UIImageView!
    @IBOutlet weak var ingredientsLabel: UILabel!
    @IBOutlet weak var descriptionText: AttributedTextView!
    
    var beer: [String : String] = [String : String]()
    var image: UIImage?
    var textColor: UIColor = UIColor.black
    var maltTitle: String = ""
    var malt: String = ""
    var hopsTitle: String = ""
    var hops: String = ""
    var yeastTitle: String = ""
    var yeast: String = ""
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        // config Navigation Bar custom
        navigationItem.title = beer["name"]
        navigationItem.leftBarButtonItem = UIBarButtonItem(image:  UIImage(named: "back"), style: .plain, target: self, action: #selector(back))
        
        // Check user mobile mode
        if #available(iOS 13.0, *) {
            changeUIColors()
        }
        
        // Set beer info
        imgBeer.image = image
        
        ingredientsLabel.text = NSLocalizedString("title_ingredients", comment: "")
        
        maltTitle = NSLocalizedString("malt", comment: "") + ": \n"
        malt = "\(beer["ingredients_malt_name"] ?? "")" + "\n" + "\(beer["ingredients_malt_value"] ?? "")" + " " + "\(beer["ingredients_malt_unit"] ?? "")"
        
        hopsTitle = "\n\n" + NSLocalizedString("hops", comment: "") + ": \n"
        hops = beer["ingredients_hops"] ?? ""
        
        yeastTitle = NSLocalizedString("yeast", comment: "") + ": \n"
        yeast = beer["ingredients_yeast"] ?? ""
        
        descriptionText.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 0);
        
        buildIngredientsText()
        
    }
    
    // Build custom Textview
    func buildIngredientsText() {
        
        descriptionText.attributer = maltTitle.color(GlobalVariables.instance().colorPrimary).fontName("Lato-Black").size(17)
            + malt.color(textColor).fontName("Lato-Medium").size(17)
            + hopsTitle.color(GlobalVariables.instance().colorPrimary).fontName("Lato-Black").size(17)
            + hops.color(textColor).fontName("Lato-Medium").size(17)
            + yeastTitle.color(GlobalVariables.instance().colorPrimary).fontName("Lato-Black").size(17)
            + yeast.color(textColor).fontName("Lato-Medium").size(17)
        
    }
    
    // Check user mobile mode
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        
        super.traitCollectionDidChange(previousTraitCollection)

        if #available(iOS 13.0, *) {
            changeUIColors()
        }
        
    }
    
    // Change UI colors for dark or light mode
    func changeUIColors() {
        
        if self.traitCollection.userInterfaceStyle == .dark {
            navigationItem.leftBarButtonItem?.tintColor = UIColor.white
            textColor = UIColor.white
            buildIngredientsText()
        }
        else {
            navigationItem.leftBarButtonItem?.tintColor = UIColor.black
            textColor = UIColor.black
            buildIngredientsText()
        }
        
    }
    
    @objc func back(sender: UIButton!) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
}
