//
//  ExtensionTableView.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation
import UIKit
import NVActivityIndicatorView
import SDWebImage

// MARK: - UITableView Data Source

extension VCBeers : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return viewModel.beerViewModel.count
        
    }
    
    private func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }

    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Pagination: load news items when it requires
        checkForLastCell(with: indexPath)
        
        let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "BeerCell") as! BeerTableViewCell
        
        // Start loading indicator
        tableViewCell.activityIndicator.startAnimating()
        
        // Set circle Imageview
        tableViewCell.urlToImage.makeRounded()
        
        tableViewCell.tintColor = GlobalVariables.instance().colorPrimary
        
        let data = viewModel.beerViewModel[indexPath.row]
        
        tableViewCell.titleLabel?.text = data.name
        tableViewCell.taglineLabel?.text = data.tagline
        
        return tableViewCell
        
    } // end cellForRowAt function
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let data = viewModel.beerViewModel[indexPath.row]
        
        // Download beer image
        (cell as? BeerTableViewCell)?.urlToImage.sd_setImage(with: URL(string: data.image!), placeholderImage: deafultImage, options: [], progress: nil, completed: {(image, error, cacheType, url) in
            (cell as? BeerTableViewCell)?.activityIndicator.stopAnimating()
        })
        
        // Insert loading spin at the end of tableview
        let lastSectionIndex = tableView.numberOfSections - 1
        let lastRowIndex = tableView.numberOfRows(inSection: lastSectionIndex) - 1

        if indexPath.section == lastSectionIndex && indexPath.row == lastRowIndex {
            let spinner = NVActivityIndicatorView(frame: CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(60)), type: .circleStrokeSpin, color: GlobalVariables.instance().colorPrimary, padding: 15)
            spinner.startAnimating()
            tableView.tableFooterView = spinner
            tableView.tableFooterView?.isHidden = false
        }
        
    } // end willDisplay function
    
    // Checking Cell
    private func checkForLastCell(with indexPath:IndexPath) {
        
        if indexPath.row == (viewModel.beerViewModel.count - 1) {
            if GlobalVariables.instance().totalItems + 1 > viewModel.beerViewModel.count {
                GlobalVariables.instance().page += 1
                pageSetup()
            }
        }
        
    }
    
} // end extension BeersViewController : UITableViewDataSource
