//
//  BeerTableViewCell.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class BeerTableViewCell: UITableViewCell {

    @IBOutlet weak var urlToImage: UIImageView!
    @IBOutlet weak var activityIndicator: NVActivityIndicatorView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var taglineLabel: UILabel!
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        titleLabel.textColor = GlobalVariables.instance().colorPrimary
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
