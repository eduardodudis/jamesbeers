//
//  GlobalVariables.swift
//  James Beers
//
//  Created by Eduardo Maia on 23/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation
import UIKit

class GlobalVariables {
    
    private static var globalVariables : GlobalVariables? = nil
    
    let apiBaseUrl: String = "https://api.punkapi.com/v2/"
    let apiBeers: String = "beers?page="
    let apiPerPage: String = "&per_page="
    let colorPrimary = UIColor(red: 137 / 255, green: 97 / 255, blue: 212 / 255, alpha: 1)
    let colorGrey = UIColor(red: 214 / 255, green: 214 / 255, blue: 214 / 255, alpha: 1)
    let beersLimitPage = 25
    var page = 1
    var totalItems = 0;
    
    static func instance() -> GlobalVariables {
        
        if (globalVariables == nil) {
            globalVariables = GlobalVariables()
        }
        
        return globalVariables!
        
    }
    
    private init() {}
    
}
