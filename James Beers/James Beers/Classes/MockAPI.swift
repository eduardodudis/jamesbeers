//
//  MockAPI.swift
//  James Beers
//
//  Created by Eduardo Maia on 24/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class MockAPI: RequestProtocol {
    
    var beers: [Beer] = []
    
    func getAllBeers(for url: String, result: @escaping (_ json: Data?) -> ()) {
        
        // Call server API
        AF.request(url, method: .get)
            .response { (response) in
                switch response.result {
                    case .success(let value):
                        if let json = value {
                            result(json)
                            return
                        }
                    break
                    case .failure(let error):
                        print("error calling GET on /\(url) -> \(error)")
                        result(nil)
                    break
                }
            }
        
    } // end func getAllBeers
    
    func fillBeers(json: Data?) -> [Beer] {
    
        // parse JSON data to Beer
        do {
            if json != nil {
                let jsonObj = try JSON(data: json!)

                // fill Beer object
                for (_, subJson) in jsonObj {
                    let maltData = Malt(name: subJson["ingredients"]["malt"][0]["name"].string ?? "",
                                        value: String(format: "%.2f", subJson["ingredients"]["malt"][0]["amount"]["value"].double ?? 0.0),
                                        unit: subJson["ingredients"]["malt"][0]["amount"]["unit"].string ?? "")
                    
                    var hopsData: [Hops] = []
                    
                    for (_, sub):(String, JSON) in subJson["ingredients"]["hops"] {
                        hopsData.append(Hops(name: sub["name"].string ?? "",
                                             value: String(format: "%.2f", sub["amount"]["value"].double ?? 0.0),
                                             unit: sub["amount"]["unit"].string ?? "",
                                             add: sub["add"].string ?? "",
                                             attribute: sub["attribute"].string ?? ""))
                    }
                    
                    let ingredientsData = Ingredients(malt: maltData,
                                                      hops: hopsData,
                                                      yeast: subJson["ingredients"]["yeast"].string ?? "")
                    
                    let beer = Beer(id: subJson["id"].int ?? 0,
                                             name: subJson["name"].string ?? "",
                                             tagline: subJson["tagline"].string ?? "",
                                             image: subJson["image_url"].string ?? "",
                                             ingredients: ingredientsData)
                    
                    beers.append(beer)
                    GlobalVariables.instance().totalItems += 1
                }
            }
        }
        catch let error {
            print("parse error: \(error.localizedDescription)")
        }
        
        return beers
        
    } // end func fillBeers
}
