//
//  Helpers.swift
//  James Beers
//
//  Created by Eduardo Maia on 25/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import Foundation
import UIKit

// MARK: - UIImageView Extension

extension UIImageView {

    // Define circle imageview
    func makeRounded() {
        
        self.layer.borderWidth = 2
        self.layer.masksToBounds = false
        self.layer.borderColor = GlobalVariables.instance().colorPrimary.cgColor
        self.layer.cornerRadius = self.frame.height / 2
        self.clipsToBounds = true
        
    }
    
} // end extension UIImageView

// MARK: - String Extension

extension String {

    // Converts a string to a percent-encoded URL, including Unicode characters.
    func encodedUrl() -> URL? {
        
        guard let decodedString = self.removingPercentEncoding,
            let unicodeEncodedString = decodedString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed),
            let components = URLComponents(string: unicodeEncodedString),
            let percentEncodedUrl = components.url else {
                return URL(string: self)
            }
        
        return percentEncodedUrl
        
    }
    
} // end extension String

// MARK: - Unit Test

extension UIViewController {
    
    // Define a preload uiviewcontroller for tests
    func preload() {
        _ = self.view
    }
    
}
