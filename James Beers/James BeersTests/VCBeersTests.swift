//
//  James_BeersTests.swift
//  James BeersTests
//
//  Created by Eduardo Maia on 23/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//

import XCTest
import UIKit
@testable import James_Beers

class VCBeersTests: XCTestCase {

    var navigationController: UINavigationController!
    var beersViewController: VCBeers!
    var beersViewModel: BeerViewModel!
    var beer: Beer!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        navigationController = UINavigationController()
        beersViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCBeers") as? VCBeers

        navigationController.setViewControllers([beersViewController], animated: false)

        beersViewController.preload()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        navigationController = nil
        beersViewController = nil
        beersViewModel = nil
        beer = nil
        
    }
    
    // Test if has tableview
    func testTableViewOutlet() {
        
        XCTAssertNotNil(beersViewController.tableView)
        
    }
    
    // Test if has tableview datasource delegate
    func testThatViewConformsToUITableViewDataSource() {
        
        XCTAssertTrue(beersViewController.conforms(to: UITableViewDataSource.self), "View does not conform to UITableView datasource protocol")
        
    }
    
    // Test if has tableview datasource
    func testThatTableViewHasDataSource() {
        
        XCTAssertNotNil(beersViewController.tableView.dataSource, "Table datasource cannot be nil")
        
    }
    
    // Test if has tableview delegate
    func testThatViewConformsToUITableViewDelegate() {
        
        XCTAssertTrue(beersViewController.conforms(to: UITableViewDelegate.self), "View does not conform to UITableView delegate protocol")
        
    }
     
    // Test if has tableview delegate connected
    func testTableViewIsConnectedToDelegate() {
        
        XCTAssertNotNil(beersViewController.tableView.delegate, "Table delegate cannot be nil")
        
    }
    
    // Test if tableview has cell right identifier
    func testTableViewHasCells() {
        
        let cell = beersViewController.tableView.dequeueReusableCell(withIdentifier: "BeerCell")

        XCTAssertNotNil(cell, "TableView should be able to dequeue cell with identifier: 'BeerCell'")
        
    }
    
    // Test API
    func testSearchAndFillBeers() {
        
        let didFinish = self.expectation(description: #function)
        
        let api = MockAPI()

        var result: Data?
        
        // Test API call
        api.getAllBeers(for: GlobalVariables.instance().apiBaseUrl + GlobalVariables.instance().apiBeers + "\(GlobalVariables.instance().page)" + GlobalVariables.instance().apiPerPage + "\(GlobalVariables.instance().beersLimitPage)") {
            result = $0
            didFinish.fulfill()
        }
        
        waitForExpectations(timeout: 3, handler: nil)
        
        XCTAssertNoThrow(result)
        
        wait(interval: 3) {
            
            // Test return from API with the same number as config in GlobalVariables
            let expectedRows = GlobalVariables.instance().beersLimitPage
            
            XCTAssertTrue(self.beersViewController.tableView(self.beersViewController.tableView, numberOfRowsInSection: 0) == expectedRows, "Table has \(self.beersViewController.tableView(self.beersViewController.tableView, numberOfRowsInSection: 0)) rows but it should have \(expectedRows)")
            
            // Test values of first row
            let cell = self.beersViewController.tableView(self.beersViewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! BeerTableViewCell

            XCTAssertNotNil(cell)
            XCTAssertEqual(cell.titleLabel.text!, "Buzz", "Data success title")
            XCTAssertEqual(cell.taglineLabel.text!, "A Real Bitter Experience.", "Data success tagline")
            
            // Test a single beer data for VCBeerDetail
            let resultBeer = self.beersViewController.viewModel.getSingleBeer(index: 0)
            
            XCTAssertTrue(resultBeer.count == 7)
            
        }
        
    } // end Test API func
    
    // func to wait until run
    func wait(interval: TimeInterval = 0.1 , completion: @escaping (() -> Void)) {
        
        let exp = expectation(description: "")
        
        DispatchQueue.main.asyncAfter(deadline: .now() + interval) {
            completion()
            exp.fulfill()
        }
        
        waitForExpectations(timeout: interval + 0.1)
        
    }

}
