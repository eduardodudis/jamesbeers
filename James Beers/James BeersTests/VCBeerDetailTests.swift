//
//  VCBeersTests.swift
//  James BeersTests
//
//  Created by Eduardo Maia on 26/01/20.
//  Copyright © 2020 Eduardo Maia. All rights reserved.
//
    
import XCTest
import UIKit
import SDWebImage
@testable import James_Beers

class VCBeerDetailTests: XCTestCase {

    var navigationController: UINavigationController!
    var detailBeersViewController: VCBeerDetail!
    var beerData = [String: String]()
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        beerData["name"] = "test_name"
        beerData["image"] = "https://images.punkapi.com/v2/192.png"
        beerData["ingredients_malt_name"] = "test_malt_name"
        beerData["ingredients_malt_value"] = "test_malt_value"
        beerData["ingredients_malt_unit"] = "test_malt_unit"
        beerData["ingredients_hops"] = "test_hops"
        beerData["ingredients_yeast"] = "test_yeast"
        
        navigationController = UINavigationController()
        detailBeersViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "VCBeerDetail") as? VCBeerDetail

        navigationController.setViewControllers([detailBeersViewController], animated: false)

        detailBeersViewController.beer = beerData
        
        detailBeersViewController.preload()
        
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        navigationController = nil
        detailBeersViewController = nil
        
    }
    
    // Test predefined label
    func testPreDefinedLabel() {
        
        XCTAssertEqual(detailBeersViewController.ingredientsLabel.text, NSLocalizedString("title_ingredients", comment: ""))
        
    }
    
    // Test title is the beer name
    func testHasTitleView() {
        
        XCTAssertEqual(detailBeersViewController.navigationItem.title, "test_name")
        
    }
    
    // Test back button
    func testHasLeftBarButtonItem() {
     
        XCTAssertNotNil(detailBeersViewController.navigationItem.leftBarButtonItem)
        
    }
    
    // Test back button action
    func testHasLeftBarButtonItemActionAssigned() {
        
        if let leftBarButtonItem = detailBeersViewController.navigationItem.leftBarButtonItem {
            XCTAssertTrue(leftBarButtonItem.action?.description == "backWithSender:")
        }
        else {
            XCTAssertTrue(false)
        }
        
    }
    
    // Test beer's info on textview
    func testBuildString() {
    
        detailBeersViewController.beer = beerData
        detailBeersViewController.buildIngredientsText()
        
        let text: String = detailBeersViewController.descriptionText.text!
        
        XCTAssertNotNil(text.contains("test_malt_name"))
        XCTAssertNotNil(text.contains("ingredients_malt_value"))
        XCTAssertNotNil(text.contains("ingredients_malt_unit"))
        XCTAssertNotNil(text.contains("ingredients_hops"))
        XCTAssertNotNil(text.contains("ingredients_yeast"))
        XCTAssertNotNil(text.contains(NSLocalizedString("malt", comment: "")))
        XCTAssertNotNil(text.contains(NSLocalizedString("hops", comment: "")))
        XCTAssertNotNil(text.contains(NSLocalizedString("yeast", comment: "")))
        
    }
    
    // Test beer image load from URL
    func testImageLoadFromURL() {
        
        let didFinish = self.expectation(description: #function)
        
        detailBeersViewController.imgBeer.sd_setImage(with: URL(string: beerData["image"]!), placeholderImage: nil, options: [], progress: nil, completed: {(image, error, cacheType, url) in
            
            self.detailBeersViewController.imgBeer.image = image
            
            if self.detailBeersViewController.imgBeer.image != nil {
                didFinish.fulfill()
            } else {
                XCTFail()
            }
        })
        
        waitForExpectations(timeout: 3.0, handler: nil)
        
    }
    
}
